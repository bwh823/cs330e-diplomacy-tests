#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from Diplomacy import *


class TestDiplomacy (TestCase):

    # TEST READ
    def test_read_1(self):
        s = "A Austin Move Barcelona"
        a, b, c, d = diplomacy_read(s)
        self.assertEqual(a, 'A')
        self.assertEqual(b, 'Austin')
        self.assertEqual(c, 'Move')
        self.assertEqual(d, 'Barcelona')

    def test_read_2(self):
        s = "A Austin Hold"
        a, b, c, d = diplomacy_read(s)
        self.assertEqual(a, 'A')
        self.assertEqual(b, 'Austin')
        self.assertEqual(c, 'Hold')
        self.assertIsNone(d)

    def test_read_3(self):
        s = ""
        with self.assertRaises(ValueError):
            diplomacy_read(s)


    # TEST PRINT
    def test_print_1(self):
        w = StringIO()
        result = 'Random Test'
        diplomacy_print(w, result)
        self.assertEqual(w.getvalue(), result + '\n')

    def test_print_2(self):
        w = StringIO()
        result = diplomacy_print(w, 'Random Test')
        self.assertIsNone(result)

    def test_print_3(self):
        w = StringIO()
        result = 'A Austin\n'
        diplomacy_print(w, result)
        output = w.getvalue()
        self.assertEqual(len(output.split()), 2)

    # TEST SOLVE
    def test_solve_1(self):
        r = StringIO("A Austin Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\n")

    def test_solve_2(self):
        r = StringIO("A Austin Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_3(self):
        r = StringIO("C Dallas Support A\nA Austin Move NewYork\nB NewYork Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A NewYork\nB [dead]\nC Dallas\n")

    def test_solve_4(self):
        r = StringIO("C Dallas Support A\nA Austin Move NewYork\nB NewYork Hold\nD FortWorth Move Dallas")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_5(self):
        r = StringIO("C Dallas Support A\nA Austin Hold\nB NewYork Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB [dead]\nC Dallas\n")


if __name__ == "__main__":
    main()  # pragma: no cover
