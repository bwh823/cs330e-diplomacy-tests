from unittest import main, TestCase
from io import StringIO

from Diplomacy import read_commands, diplomacy_solve, write_report, diplomacy

class TestDiplomacy(TestCase):

    # read function
    # input: reader
    # output: nested list of strings

    def test_read1(self):
        r = StringIO("A Madrid Hold")
        self.assertEqual(read_commands(r),
                         [["A", "Madrid", "Hold"]])

    def test_read2(self):
        r = StringIO("A Madrid Hold\n")
        self.assertEqual(read_commands(r),
                         [["A", "Madrid", "Hold"]])

    def test_read3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        self.assertEqual(read_commands(r),
                         [["A", "Madrid", "Hold"],
                          ["B", "Barcelona", "Move", "Madrid"],
                          ["C", "London", "Support", "B"]])
    
    # solve function
    # input: nested list of strings
    # output: nested list of strings
    
    def test_solve1(self):
        self.assertEqual(diplomacy_solve([["A", "Madrid", "Hold"]]),
                         [["A", "Madrid"]])

    def test_solve2(self):
        self.assertEqual(diplomacy_solve([["A", "Madrid", "Hold"],
                                          ["B", "Barcelona", "Move", "Madrid"],
                                          ["C", "London", "Support", "B"]]),
                         [["A", "[dead]"],
                          ["B", "Madrid"],
                          ["C", "London"]])

    def test_solve3(self):
        self.assertEqual(diplomacy_solve([["A", "Madrid", "Hold"],
                                          ["B", "Barcelona", "Move", "Madrid"]]),
                         [["A", "[dead]"],
                          ["B", "[dead]"]])

    def test_solve4(self):
        self.assertEqual(diplomacy_solve([["A", "Madrid", "Support", "B"],
                                          ["B", "London", "Hold"],
                                          ["C", "Paris", "Move", "Madrid"]]),
                         [["A", "[dead]"],
                          ["B", "London"],
                          ["C", "[dead]"]])

    def test_solve5(self):
        self.assertEqual(diplomacy_solve([["A", "Tokyo", "Hold"],
                                          ["B", "Beijing", "Support", "A"],
                                          ["C", "London", "Support", "B"],
                                          ["D", "Austin", "Move", "Beijing"],
                                          ["E", "NewYork", "Move", "Tokyo"]]),
                         [["A", "[dead]"],
                          ["B", "Beijing"],
                          ["C", "London"],
                          ["D", "[dead]"],
                          ["E", "[dead]"]])

    def test_solve6(self):
        self.assertEqual(diplomacy_solve([["A", "Tokyo", "Hold"],
                                          ["B", "Beijing", "Support", "A"],
                                          ["C", "London", "Support", "B"],
                                          ["D", "Austin", "Move", "Tokyo"],
                                          ["E", "NewYork", "Support", "D"]]),
                         [["A", "[dead]"],
                          ["B", "Beijing"],
                          ["C", "London"],
                          ["D", "[dead]"],
                          ["E", "NewYork"]])

    # write function
    # input: nested list of strings
    # output: writer

    def test_write1(self):
        w = StringIO()
        write_report(w, [["A", "Madrid"]])
        self.assertEqual(w.getvalue(),
                         "A Madrid\n")

    def test_write2(self):
        w = StringIO()
        write_report(w, [["A", "Madrid"], ["B", "Barcelona"], ["C", "[dead]"]])
        self.assertEqual(w.getvalue(),
                         "A Madrid\nB Barcelona\nC [dead]\n")

    def test_write3(self):
        w = StringIO()
        write_report(w, [["A", "[dead]"], ["B", "[dead]"]])
        self.assertEqual(w.getvalue(),
                         "A [dead]\nB [dead]\n")

    # main diplomacy function
    # input: reader
    # output: writer

    def test_main1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy(r,w)
        self.assertEqual(w.getvalue(),
                         "A Madrid\n")

    def test_main2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy(r,w)
        self.assertEqual(w.getvalue(),
                         "A Barcelona\nB Madrid\n")

    def test_main3(self):
        r = StringIO("A Boston Support C\nB Austin Hold\nC Chicago Move Austin\n")
        w = StringIO()
        diplomacy(r,w)
        self.assertEqual(w.getvalue(),
                         "A Boston\nB [dead]\nC Austin\n")


if __name__ == "__main__":
    main()
