
from io import StringIO
from unittest import main, TestCase

from Diplomacy import Army, diplomacy_solve, gameStart, support, fight, diplomacyPrint


class TestDiplomacy (TestCase):
    # ----
    # testing gameStart function
    # ----

    def test_gameStart(self):
        inputData = ['A', 'Paris', 'Hold']
        s = gameStart(inputData)
        self.assertEqual("A", s.name)
        self.assertEqual("Paris", s.city)
        self.assertEqual("Hold", s.action)

    def test_gameStart2(self):
        inputData = ['B', 'Barcelona', 'Move', 'Austin']
        s = gameStart(inputData)
        self.assertEqual("B", s.name)
        self.assertEqual("Austin", s.city)
        self.assertEqual("Move", s.action)
        self.assertEqual(None, s.modifier)

    def test_gameStart3(self):
        inputData = ['C', 'London', 'Support', 'D']
        s = gameStart(inputData)
        self.assertEqual("C", s.name)
        self.assertEqual("London", s.city)
        self.assertEqual("Support", s.action)
        self.assertEqual("D", s.modifier)
        self.assertEqual(0, s.value)
        self.assertEqual(False, s.dead)

    # -----
    # testing diplomacy_solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual( w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid B\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


if __name__ == "__main__":  # pragma: no cover
    main()

