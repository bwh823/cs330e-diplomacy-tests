#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Testdiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# Testdiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    def test_read_1(self):
        s = StringIO('A Paris Hold\n')
        i = diplomacy_read(s)
        self.assertEqual(i,  [['A', 'Paris', 'Hold']])

    def test_read_2(self):
        s = StringIO('B Vietnam Move Paris\n')
        i = diplomacy_read(s)
        self.assertEqual(i,  [['B', 'Vietnam', 'Move', 'Paris']])
    
    def test_read_3(self):
        s = StringIO('C Thailand Support B\n')
        i = diplomacy_read(s)
        self.assertEqual(i,  [['C', 'Thailand', 'Support', 'B']])

    def test_read_4(self):
        s = StringIO('A Paris Hold\nB Vietnam Move Paris\nC Thailand Support B\n')
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Paris', 'Hold'],
                             ['B', 'Vietnam', 'Move', 'Paris'],
                             ['C', 'Thailand', 'Support', 'B']])


    # -----
    # eval
    # -----

    def test_eval_1(self):
        v = diplomacy_eval([['A', 'Paris', 'Hold']])
        self.assertEqual(v, [['A', 'Paris']])

    def test_eval_2(self):
        v = diplomacy_eval([['A', 'Paris', 'Hold'],
                            ['B', 'Vietnam', 'Move', 'Paris'],
                            ['C', 'Thailand', 'Support', 'B']])
        self.assertEqual(v, [['A', '[dead]'], 
                            ['B', 'Paris'], 
                            ['C', 'Thailand']])

    def test_eval_3(self):
        v = diplomacy_eval([['A', 'Paris', 'Hold'],
                            ['B', 'Vietnam', 'Move', 'Paris']])
        self.assertEqual(v, [['A', '[dead]'],
                             ['B', '[dead]']])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [['A', 'Paris']])
        self.assertEqual(w.getvalue(), 'A Paris\n')

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [['A', '[dead]'],
                            ['B', 'Paris'],
                            ['C', 'Thailand']])
        self.assertEqual(w.getvalue(), 'A [dead]\nB Paris\nC Thailand\n')

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [['A', '[dead]'],
                            ['B', '[dead]'],
                            ['C', '[dead]']] )
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\n')

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO('A Paris Hold\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Paris\n')

    def test_solve_2(self):
        r = StringIO('A Paris Hold\nB Vietnam Move Paris\nC Thailand Support B\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Paris\nC Thailand\n')

    def test_solve_3(self):
        r = StringIO('A Paris Hold\nB Vietnam Move Paris\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch Testdiplomacy.py >  Testdiplomacy.out 2>&1


$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> Testdiplomacy.out



$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
diplomacy.py          12      0      2      0   100%
Testdiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
