#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval ,diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "C Paris Moves B\nA Austin Hold\nB Madrid Support A"
        x = diplomacy_read(s)
        self.assertEqual(x,  ["C Paris Moves B",
                              "A Austin Hold",
                              "B Madrid Support A"])

    def test_read_2(self):
        s = "A Minneapolis Hold\nB Bangkok Move Minneapolis\nC Cairo Support B"
        x = diplomacy_read(s)
        self.assertEqual(x,  ["A Minneapolis Hold",
                              "B Bangkok Move Minneapolis",
                              "C Cairo Support B"])

    def test_read_3(self):
        s = "A Austin Move Paris\nB Barcelona Move Paris\nC Tokyo Move Barcelona\nD Seattle Move Tokyo"
        x = diplomacy_read(s)
        self.assertEqual(x,  ["A Austin Move Paris",
                              "B Barcelona Move Paris",
                              "C Tokyo Move Barcelona",
                              "D Seattle Move Tokyo"])
        
    # -----
    # eval
    # -----

    def test_eval_1(self):
        t = diplomacy_eval(["A Madrid Hold",
                            "B Barcelona Move Madrid"])
        self.assertEqual(t, ["A [dead]","B [dead]"])

    def test_eval_2(self):
        t = diplomacy_eval(["A NewYork Hold",
                            "B Boston Move NewYork",
                            "C Vancouver Move NewYork",
                            "D Perth Support B",
                            "E Dublin Support A"])
        self.assertEqual(t, ["A [dead]","B [dead]","C [dead]","D Perth","E Dublin"])

    def test_eval_3(self):
        t = diplomacy_eval(["A Paris Hold",
                             "B London Support A",
                             "C Madrid Move London",
                             "D Tokyo Move Paris"])
        self.assertEqual(t, ["A [dead]","B [dead]","C [dead]","D [dead]"])
        

    def test_eval_4(self):
        t = diplomacy_eval(["A Bath Hold",
                             "B Berlin Move Bath",
                             "C Shanghai Support B"])
        self.assertEqual(t, ["A [dead]","B Bath","C Shanghai"])


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]","B [dead]"])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]","B [dead]","C [dead]","D [dead]"])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]","B Bath","C Shanghai"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Bath\nC Shanghai\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Bath Hold\nB Berlin Move Bath\nC Shanghai Support B\nD Helsinki Hold\nE Dallas Hold\nF Chicago Move Berlin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Bath\nC Shanghai\nD Helsinki\nE Dallas\nF Berlin\n")

    def test_solve_2(self):
        r = StringIO("A Phoenix Hold\nB Houston Move Phoenix\nC NewOrleans Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Phoenix\nC NewOrleans\n")

    def test_solve_3(self):
        r = StringIO("A SanFrancisco Hold\nB Brooklyn Hold\nC Bronx Move SanFrancisco")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Brooklyn\nC [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py        12      0      2      0   100%
TestDiplomacy.py    32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
