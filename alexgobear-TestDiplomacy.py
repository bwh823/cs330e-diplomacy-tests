from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve
from Army import Army
from City import City

# -------------
# TestDiplomacy
# -------------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----
    def test_read_1(self):
        f = diplomacy_read(
            StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"))

        madrid = City('Madrid')
        barcelona = City('Barcelona')
        london = City('London')

        A = Army('A', madrid)
        B = Army('B', barcelona)
        C = Army('C', london)

        armies = [A, B, C]
        army_actions = [['Hold\n'], ['Move', 'Madrid\n'], ['Support', 'B\n']]
        army_dict = {
            'A': A,
            'B': B,
            'C': C
        }
        city_dict = {
            'Madrid': madrid,
            'Barcelona': barcelona,
            'London': london
        }

        self.assertEqual(f, (armies, army_actions, army_dict, city_dict))

    def test_read_2(self):
        f = diplomacy_read([])

        self.assertEqual(f, ([], [], {}, {}))

    def test_read_3(self):
        f = diplomacy_read(
            StringIO('A Barcelona oh\nG Beijing no way\nP Atlantis swim\n'))

        barcelona = City('Barcelona')
        beijing = City('Beijing')
        atlantis = City('Atlantis')

        A = Army('A', barcelona)
        G = Army('G', beijing)
        P = Army('P', atlantis)

        armies = [A, G, P]
        army_actions = [['oh\n'], ['no', 'way\n'], ['swim\n']]
        army_dict = {
            'A': A,
            'G': G,
            'P': P
        }
        city_dict = {
            'Barcelona': barcelona,
            'Beijing': beijing,
            'Atlantis': atlantis
        }

        self.assertEqual(f, (armies, army_actions, army_dict, city_dict))

    # ----
    # eval
    # ----

    def test_eval_1(self):
        self.assertIsNone(diplomacy_eval([], [], {}, {}))

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_4(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        )

    def test_solve_5(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n"
        )

    def test_solve__6(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n"
        )

    def test_solve_7(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        )

    def test_solve_8(self):
        # custom test case (the Madrid attack): A's hold is supported by E. B's attack is supported by D and F. C's attack is not supported.

        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF SanAntonio Support B"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Austin\nF SanAntonio\n"
        )

    def test_solve_9(self):
        # custom test case (not highest support values tied): A's hold is supported by E and G. B's attack is supported by D and F and H. C's attack is not supported.

        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF SanAntonio Support B\nG Houston Support A\nH Laredo Support B"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Austin\nF SanAntonio\nG Houston\nH Laredo\n"
        )

    def test_solve_10(self):
        # special corner case that revealed bug (support evaluation decrementing support multiple times)

        r = StringIO(
            "A Austin Move SanAntonio\nB Dallas Support A\nC Houston Move Dallas\nD SanAntonio Support C\nE ElPaso Move Austin\nF Waco Move Austin\n"
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n"
        )

    def test_solve_11(self):
        # tests moves to undefined cities

        r = StringIO(
            'A Chicago Move Dayton\nB Denver Move Dayton\n'
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_12(self):
        # new corner case (swap city case)

        r = StringIO(
            'A Austin Move SanAntonio\nB SanAntonio Move Austin\n'
        ) 
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A SanAntonio\nB Austin\n")

    def test_solve_13(self):
        r = StringIO(
            'A Barcelona Move Madrid\nB Madrid Move Barcelona\nC Lisbon Move Barcelona\nD Vienna Move Barcelona\nE Berlin Support D\n'
        )
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\nB [dead]\nC [dead]\nD Barcelona\nE Berlin\n')
        

    # ----
    # print
    # ----

    def test_print_1(self):
        madrid = City('Madrid')
        barcelona = City('Barcelona')
        london = City('London')

        A = Army('A', madrid)
        B = Army('B', barcelona)
        C = Army('C', london)

        armies = [A, B, C]

        w = StringIO()

        diplomacy_print(w, armies)
        self.assertEqual(
            w.getvalue(), 'A Madrid\nB Barcelona\nC London\n'
        )

# ----
# main
# ----
if __name__ == "__main__":  # pragma: no coverage
    main()
